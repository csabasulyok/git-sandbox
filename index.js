import express from 'express';

console.log('Hello World');
console.log('Conflict bait');

const app = express();

app.get('/hello', (req, res) => {
  res.send({ message: 'Hello World' });
});

app.listen(8080);
